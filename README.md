# Kubernetes Configuration Management with Kustomize

## Overview

This repository manages Kubernetes configurations using [Kustomize](https://kustomize.io/). Kustomize facilitates a template-free and declarative approach to handling application configurations, making it easier to manage distinct Kubernetes configurations across various environments.

## What is Kustomize?

Kustomize is a Kubernetes native configuration management tool that enhances the declarative application management in Kubernetes. It lets you customize raw, template-free YAML files for multiple purposes, leaving the original YAML untouched and usable as is.

Kustomize uses a base and overlays approach, where the base contains the common configuration across environments, and overlays contain environment-specific adjustments.

## Project Structure

### `base`

The `base` directory contains generic Kubernetes resources that form the foundation of the application setup. These configurations are common across all environments.

- **dev**: Specific configurations for the development environment.
- **prod**: Specific configurations for the production environment.
- **staging**: Specific configurations for the staging environment.

Each environment sub-directory typically includes:

- `deployment.yaml`: Base deployment configurations.
- `ingress.yml`: Ingress rules for routing and load balancing.
- `namespace.yaml`: Namespace definitions.
- `service.yaml`: Service definitions for exposing applications.

### `components`

The `components` directory holds modular and reusable configurations. These components can be shared across different services or environments, making the configurations DRY (Don't Repeat Yourself).

- **dev/backend** and **dev/frontend**: Specific backend and frontend configurations for the development environment.
- **prod/backend** and **prod/frontend**: Specific backend and frontend configurations for the production environment.
- **staging/backend** and **staging/frontend**: Specific backend and frontend configurations for the staging environment.

Common files in `components`:

- `config.properties`: Configuration properties.
- `cred.json`: Image registry credentials.
- `kustomization.yaml`: Kustomize configuration for managing components.
- `secret.properties`: Secret configurations.

### `overlays`

The `overlays` directory contains environment-specific customizations. Overlays modify the base configurations for specific deployment scenarios.

- **dev/backend** and **dev/frontend**: Overlays for different services in the development environment.
- **production/backend** and **production/frontend**: Overlays for different services in the production environment.
- **staging/backend** and **staging/frontend**: Overlays for different services in the staging environment.

Each service typically has its own `kustomization.yaml` for customization.

### Directory Structure Overview

```plaintext
├── base
│   ├── dev
│   │   ├── deployment.yaml
│   │   ├── ingress.yml
│   │   ├── kustomization.yaml
│   │   ├── namespace.yaml
│   │   └── service.yaml
│   ├── prod
│   │   ├── deployment.yaml
│   │   ├── ingress.yml
│   │   ├── kustomization.yaml
│   │   ├── namespace.yaml
│   │   └── service.yaml
│   └── staging
│       ├── deployment.yaml
│       ├── ingress.yml
│       ├── kustomization.yaml
│       ├── namespace.yaml
│       └── service.yaml
├── components
│   ├── dev
│   │   ├── backend
│   │   │   ├── config.properties
│   │   │   ├── cred.json
│   │   │   ├── kustomization.yaml
│   │   │   └── secret.properties
│   │   └── frontend
│   │       ├── config.properties
│   │       └── kustomization.yaml
│   ├── prod
│   │   ├── backend
│   │   │   ├── config.properties
│   │   │   ├── cred.json
│   │   │   ├── kustomization.yaml
│   │   │   └── secret.properties
│   │   └── frontend
│   │       ├── config.properties
│   │       └── kustomization.yaml
│   └── staging
│       ├── backend
│       │   ├── config.properties
│       │   ├── cred.json
│       │   ├── kustomization.yaml
│       │   └── secret.properties
│       └── frontend
│           ├── config.properties
│           └── kustomization.yaml
└── overlays
    ├── dev
    │   ├── backend
    │   │   ├── order-service-be
    │   │   │   └── kustomization.yaml
    │   │   ├── product-service-be
    │   │   │   └── kustomization.yaml
    │   │   └── user-service-be
    │   │       └── kustomization.yaml
    │   └── frontend
    │       └── react-demo-app-fe
    │           └── kustomization.yaml
    ├── production
    │   ├── backend
    │   │   ├── order-service-be
    │   │   │   └── kustomization.yaml
    │   │   ├── product-service-be
    │   │   │   └── kustomization.yaml
    │   │   └── user-service-be
    │   │       └── kustomization.yaml
    │   └── frontend
    │       └── react-demo-app-fe
    │           └── kustomization.yaml
    └── staging
        ├── backend
        │   ├── order-service-be
        │   │   └── kustomization.yaml
        │   ├── product-service-be
        │   │   └── kustomization.yaml
        │   └── user-service-be
        │       └── kustomization.yaml
        └── frontend
            └── react-demo-app-fe
                └── kustomization.yaml
```

## Getting Started with Kustomize

### Prerequisites

- A Kubernetes cluster.
- [Kustomize](https://kustomize.io/) installed.

### Applying Configurations

To apply configurations:

1. Navigate to the desired overlay directory.
2. Run the following command to build and apply the configuration:

   ```shell
   kustomize build . | kubectl apply -f -
   ```
