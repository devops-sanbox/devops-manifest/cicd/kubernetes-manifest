## Base Directory

The `base` directory contains the fundamental building blocks of the Kubernetes manifests. These are common resources that are shared across different environments (dev, prod, staging). Each sub-directory within `base` is environment-specific and includes essential Kubernetes objects such as Deployments, Services, Ingresses, etc.

### Structure:

- `base/`
  - `dev/`
    - `deployment.yaml`: Defines the deployment configuration for the development environment.
    - `ingress.yml`: Ingress resource for routing traffic in the development environment.
    - `kustomization.yaml`: Kustomize configuration for managing resources in the development environment.
    - `namespace.yaml`: Namespace definition for the development environment.
    - `service.yaml`: Service resource for exposing applications in the development environment.
  - `prod/`
    - Similar structure to `dev/`, but tailored for the production environment.
  - `staging/`
    - Similar structure to `dev/`, but tailored for the staging environment.
