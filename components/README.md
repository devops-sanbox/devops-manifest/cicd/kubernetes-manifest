## Components Directory

The `components` directory contains reusable configurations that can be shared across different services or environments. Each sub-directory within `components` represents a logical grouping of configurations, such as backend and frontend settings for different environments.

### Structure:

- `components/`
  - `dev/`
    - `backend/`
      - `config.properties`: Configuration properties specific to the development environment's backend.
      - `cred.json`: Credential file for the development environment's backend.
      - `kustomization.yaml`: Kustomize configuration for managing the backend in the development environment.
      - `secret.properties`: Secret configuration for the development environment's backend.
    - `frontend/`
      - Similar structure to `backend/`, but for frontend configurations.
  - `prod/` and `staging/`
    - Similar structure to `dev/`, but tailored for the production and staging environments, respectively.
