## Overlays Directory

The `overlays` directory contains environment-specific customizations on top of the base configurations. It allows for variations in different environments, such as dev, prod, and staging. Each sub-directory within `overlays` represents a specific environment and contains further sub-directories for backend and frontend customizations for different services.

### Structure:

- `overlays/`
  - `dev/`
    - `backend/`
      - `order-service-be/`
        - `kustomization.yaml`: Kustomize configuration for customizing the order service backend in the development environment.
      - `product-service-be/` and `user-service-be/`
        - Similar structure to `order-service-be/`, but for different services.
    - `frontend/`
      - `react-demo-app-fe/`
        - `kustomization.yaml`: Kustomize configuration for customizing the frontend React demo app in the development environment.
  - `production/` and `staging/`
    - Similar structure to `dev/`, but tailored for the production and staging environments, respectively.
